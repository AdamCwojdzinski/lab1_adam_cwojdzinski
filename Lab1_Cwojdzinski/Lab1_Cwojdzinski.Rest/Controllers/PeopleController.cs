﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab1_Cwojdzinski.Rest.Context;
using Lab1_Cwojdzinski.Rest.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Lab1_Cwojdzinski.Rest.Controllers
{
    [ApiController]
    [Route("[people]")]
    public class PeopleController : ControllerBase
    {

        private readonly AzureDbContext _azureDbContext;

        public PeopleController(AzureDbContext azureDbContext)
        {
            _azureDbContext = azureDbContext;
        }
        // GET: People
        [HttpGet]
        public IEnumerable<Person> Index()
        {
            return _azureDbContext.People;
        }

        //GET: PeopleById
        [HttpGet("{id}")]
        public async Task<ActionResult<Person>> GetPersonById(int id)
        {
            var person = await _azureDbContext.People.FindAsync(id);

            if(person == null)
            {
                return NotFound();
            }

            return person;
        }

        //POST: People
        [HttpPost]
        public async Task<ActionResult<Person>> AddNewPerson(Person person)
        {
            _azureDbContext.People.Add(person);
            await _azureDbContext.SaveChangesAsync();

            return CreatedAtAction(nameof(GetPersonById), new { id = person.PersonId }, person);
        }

        // DELETE: 
        [HttpDelete("{id}")]
        public async Task<ActionResult<Person>> DeletePerson(int id)
        {
            var person = await _azureDbContext.People.FindAsync(id);
            if (person == null)
            {
                return NotFound();
            }

            _azureDbContext.People.Remove(person);
            await _azureDbContext.SaveChangesAsync();

            return person;
        }

        public async Task<bool> PersonExists(int id)
        {
            var person = await _azureDbContext.People.FindAsync(id);

            return person != null;
        }

        // PUT: 
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPerson(int id, Person person)
        {
            if (id != person.PersonId)
            {
                return BadRequest();
            }

            _azureDbContext.Entry(person).State = EntityState.Modified;

            try
            {
                await _azureDbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (await PersonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
    }
}